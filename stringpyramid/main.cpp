#include <stdlib.h>
#include <stdio.h>
#include <iostream>

using namespace std;

int main() {

    int height;

    cout << "Etage de la pyramide ? : ";
    cin >> height;

    for (int i = 1, k = 0; i <= height; i++, k = 0)
    {
        for (int space = 1; space <= height - i; space++)
        {
            cout << " ";
        }

        while (k != 2*i-1)
        {
            cout << "*";
            k++;
        }
        cout << endl;
    }
}